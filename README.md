# Twisted Irc Client
![1.png](https://bitbucket.org/repo/LoMy6b/images/3420117672-1.png)

## Usage

* Chechout the source **git clone https://bitbucket.org/magic_cookie/twisted-irc-client.git**
* Edit **config.py**
* Execute **python main.py**

## Dependencies

* [Python2.7](http://www.python.org/download/releases/2.7.3/)
* [Twisted](http://twistedmatrix.com/trac/)
* [PySide](http://qt-project.org/wiki/PySide) or [PyQT](http://www.riverbankcomputing.co.uk/software/pyqt/intro)

## Includes

* [txWS](https://github.com/MostAwesomeDude/txWS)
* [jQuery UI](http://jqueryui.com/)
* [Tempita](http://pypi.python.org/pypi/Tempita)