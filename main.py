# -*- coding: utf-8 -*-
#!/usr/bin/python

from twisted.words.protocols import irc
from twisted.web.server import Site
from twisted.web import resource
from twisted.internet import ssl, reactor, protocol, defer
from txws import WebSocketFactory
from http_proxy_connect import *
from multiprocessing import Process
from cgi import escape
import os, sys, re, json, inspect, tempita, time

class Tw_Server_Resource(resource.Resource):
    isLeaf = True
    def getChild(self, name, request):
        if name == '':
            return self
        return Resource.getChild(self, name, request)

    def render_GET(self, request):
        root0 = os.path.join(
                             os.path.dirname(os.path.abspath(__file__)),
                             "iface"
                             )
        root = os.path.join(root0, 'templates')
        path = '/'.join(request.postpath)
        if path == "index.html" or path == '':
            tmpl = tempita.Template.from_filename( 
                                                  os.path.join(root,
                                                               "index.html"),
                                                  namespace = filters,
                                                  encoding = None
                                                  )
            tab_buttons = [(k,v[0]) for (k,v) in id_chan_srv_dict.iteritems()]
            return tmpl.substitute(tab_buttons=tab_buttons)
        else:
            return open( os.path.join(root0,path), "rb").read() #FIXME Secure read

global ws_instance
global id_chan_srv_dict
global STATUS
STATUS = []

ROOTNAME = "-root"

filters = {
    "test": lambda s: test(s)
}

def test(s):
    return s+"test" 

def get_channel_id(channel, irc_instance):
    global id_chan_srv_dict
    d = dict([(v,k) for (k,v) in id_chan_srv_dict.iteritems()])[(channel, irc_instance)]
    return d

def fill_stream(msg, tab):
    if msg:
        msg = msg.encode('utf-8')
        tuple_srv_chan = id_chan_srv_dict[tab]
        tuple_srv_chan[1].send_stream(tuple_srv_chan[0], msg)

def fill_recv(data, channel_id, msgtype = 'message'):
    html_wrapper = Html_Wrapper()
    if msgtype == "addtab":
        data = json.dumps( {"new_tab" : html_wrapper.tab_li( (data, str(channel_id)) ), "new_div" : html_wrapper.tab_div(str(channel_id)) } )
    else:
        data = '"{0}"'.format(data)
    try:
        ws_instance.send_to_browser(data, channel_id, msgtype)
    except:
        pass

def escape_colorize(string):
    return colorize(escape(string).replace('\\', '&#92;').replace('"','&quot;'))

color_pattern = re.compile(r"(\x03[0-9]{1,2},[0-9]{1,2}|\x03[0-9]{1,2}|\x03|\x16|\x02|\x1f|\x0f).*?")

def colorize(string):
    string = re.sub(color_pattern, r"", string)
    return string

def handle_commands(command, tab, params = ''):

    if command[0:2] == "//":
        fill_stream(command[1:].encode("utf-8") + ' '.join( [_.encode("utf-8") for _ in params ] ), tab )

    elif command.lower() == "/nick":
        if params:
            id_chan_srv_dict[tab][1].setNick(params[0].encode("utf-8"))
            id_chan_srv_dict[tab][1].nickname = params[0].encode("utf-8")
        else:
            handle_user_errors()

    elif command.lower() == "/quit":
        if params:
            id_chan_srv_dict[tab][1].quit(' '.join( [_.encode("utf-8") for _ in params ] ))
        else:   
            id_chan_srv_dict[tab][1].quit()

    elif command.lower() == "/join":
        if params:
            new_channel = params[0].encode("utf-8")
            if not (new_channel, id_chan_srv_dict[tab][1]) in id_chan_srv_dict.values():
                new_tab = max(id_chan_srv_dict) + 1
                fill_recv(new_channel, new_tab, msgtype = 'addtab')
                id_chan_srv_dict.update({new_tab : (new_channel, id_chan_srv_dict[tab][1])})
                id_chan_srv_dict[tab][1].factory.chanlist.append(new_channel)
                id_chan_srv_dict[tab][1].join(new_channel)
            else:
                handle_user_errors()
        else:
            handle_user_errors()

    elif command.lower() == "/part":
        if not params:
            part_channel = id_chan_srv_dict[tab][0]
            fill_recv(part_channel, tab, msgtype = 'removetab')
            try:
                id_chan_srv_dict[tab][1].exit_channel(part_channel)
            except:
                pass

            del id_chan_srv_dict[tab]
        else:
            handle_user_errors()

    elif command.lower() == "/say":
        if params:
            channel = id_chan_srv_dict[tab][0]
            mesg = ' '.join( [_.encode("utf-8") for _ in params ] )
            id_chan_srv_dict[tab][1].msg(channel, mesg)
            fill_recv( id_chan_srv_dict[tab][1].html_wrapper.privmsg((channel, mesg)), get_channel_id( channel, id_chan_srv_dict[tab][1] ) )
        else:
            handle_user_errors()        

    elif command.lower() == "/msg":
        if len(params) > 1:
            user = params[0].encode("utf-8")
            mesg = ' '.join( [_.encode("utf-8") for _ in params[1:] ] )
            id_chan_srv_dict[tab][1].msg(user, mesg)
        else:
            handle_user_errors()

    elif command.lower() == "/topic":
        if params:
            channel = id_chan_srv_dict[tab][0]
            new_topic = ' '.join( [_.encode("utf-8") for _ in params] )
            try:
                id_chan_srv_dict[tab][1].topic(channel, new_topic)
            except:
                pass    
        else:
            handle_user_errors()

    elif command.lower() == "/me":
        channel = id_chan_srv_dict[tab][0]
        action = ' '.join( [_.encode("utf-8") for _ in params] )
        id_chan_srv_dict[tab][1].msg(channel, "\x01ACTION {0}\x01".format(action) )
        id_chan_srv_dict[tab][1].action(id_chan_srv_dict[tab][1].nickname, channel, action)

    elif command.lower() == "/query":
        if params:
            user = params[0].encode("utf-8")
            try:
                mesg = ' '.join( [_.encode("utf-8") for _ in params[1:]] )
            except:
                mesg = ''   
            if user in [ _[0] for _ in id_chan_srv_dict.values() ]:
                    if msg:
                        id_chan_srv_dict[tab][1].msg(user, mesg)
                        fill_recv( id_chan_srv_dict[tab][1].html_wrapper.privmsg((user, mesg)), get_channel_id( user, id_chan_srv_dict[tab][1] )  )
            else:
                new_tab = max(id_chan_srv_dict) + 1
                id_chan_srv_dict.update( { new_tab : (user, id_chan_srv_dict[tab][1]) } )
                fill_recv( user, new_tab, msgtype = 'addtab' )
                if mesg:
                    id_chan_srv_dict[tab][1].msg(user, mesg)
                    fill_recv( id_chan_srv_dict[tab][1].html_wrapper.privmsg((user, mesg)), get_channel_id( user, id_chan_srv_dict[tab][1] ) )
        #FIXME No such nick/channel handle
        else:
            handle_user_errors()
    else:
        handle_user_errors()

def controller(data, tab):

    if data == "exit_channel":
        try:
            tab = int(tab)
            channel = id_chan_srv_dict[tab][0]
            id_chan_srv_dict[tab][1].exit_channel(channel)
        except:
            pass    
        del id_chan_srv_dict[tab]
    elif data == "request_nicklist":
        try:
            tab = int(tab)
            channel = id_chan_srv_dict[tab][0]
            id_chan_srv_dict[tab][1].names(channel).addCallback(
                                            id_chan_srv_dict[tab][1].got_names,
                                            channel
                                            )
        except:
            pass    
    elif data == "request_topic":
        try:
            tab = int(tab)
            channel = id_chan_srv_dict[tab][0]
            id_chan_srv_dict[tab][1].get_topic(channel)
        except:
            pass
    elif data == "request_status":
        global STATUS
        html_wrapper = Html_Wrapper()
        for line in STATUS:
            fill_recv( html_wrapper.privmsg(line), ROOTNAME )   
    else:
        print "Controller call", data, tab

def handle_user_errors(): #FIXME
    pass

class Html_Wrapper():

    def __init__(self):
        self.root = "iface/templates/events/"

    def wrap_string(self, string):
        timestamp = time.strftime("[%H:%M] ", time.localtime(time.time()))
        str0 = escape_colorize(string)
        tmpl = tempita.Template.from_filename(self.root+inspect.stack()[0][3], namespace=filters, encoding=None)
        return tmpl.substitute(locals()).replace("\n", "")

    def privmsg(self, string):
        timestamp = time.strftime("[%H:%M] ", time.localtime(time.time()))
        str0 = escape_colorize(string[0])
        str1 = escape_colorize(string[1])
        tmpl = tempita.Template.from_filename(self.root+inspect.stack()[0][3], namespace=filters, encoding=None)
        return tmpl.substitute(locals()).replace("\n", "")

    def privmsg_highlight(self,string): #FIXME: add highlight class
        timestamp = time.strftime("[%H:%M] ", time.localtime(time.time()))
        str0 = escape_colorize(string[0])
        str1 = escape_colorize(string[1])       
        tmpl = tempita.Template.from_filename(self.root+inspect.stack()[0][3], namespace=filters, encoding=None)
        return tmpl.substitute(locals()).replace("\n", "")

    def join(self, string):
        timestamp = time.strftime("[%H:%M] ", time.localtime(time.time()))
        str0 = escape_colorize(string[0])
        str1 = escape_colorize(string[1])
        tmpl = tempita.Template.from_filename(self.root+inspect.stack()[0][3], namespace=filters, encoding=None)
        return tmpl.substitute(locals()).replace("\n", "")

    def part(self, string):
        timestamp = time.strftime("[%H:%M] ", time.localtime(time.time()))
        str0 = escape_colorize(string[0])
        str1 = escape_colorize(string[1])       
        tmpl = tempita.Template.from_filename(self.root+inspect.stack()[0][3], namespace=filters, encoding=None)
        return tmpl.substitute(locals()).replace("\n", "")

    def quit(self, string):
        timestamp = time.strftime("[%H:%M] ", time.localtime(time.time()))
        str0 = escape_colorize(string[0])
        str1 = escape_colorize(string[1])
        tmpl = tempita.Template.from_filename(self.root+inspect.stack()[0][3], namespace=filters, encoding=None)
        return tmpl.substitute(locals()).replace("\n", "")

    def kicked(self, string):
        timestamp = time.strftime("[%H:%M] ", time.localtime(time.time()))
        str0 = escape_colorize(string[0])
        str1 = escape_colorize(string[1])
        str2 = escape_colorize(string[2])
        str3 = escape_colorize(string[3])
        tmpl = tempita.Template.from_filename(self.root+inspect.stack()[0][3], namespace=filters, encoding=None)
        return tmpl.substitute(locals()).replace("\n", "")

    def me_kicked(self, string):
        timestamp = time.strftime("[%H:%M] ", time.localtime(time.time()))
        str0 = escape_colorize(string[0])
        str1 = escape_colorize(string[1])
        str2 = escape_colorize(string[2])
        str3 = escape_colorize(string[3])
        tmpl = tempita.Template.from_filename(self.root+inspect.stack()[0][3], namespace=filters, encoding=None)
        return tmpl.substitute(locals()).replace("\n", "")

    def nickchange(self, string):
        timestamp = time.strftime("[%H:%M] ", time.localtime(time.time()))
        str0 = escape_colorize(string[0])
        str1 = escape_colorize(string[1])
        tmpl = tempita.Template.from_filename(self.root+inspect.stack()[0][3], namespace=filters, encoding=None)
        return tmpl.substitute(locals()).replace("\n", "")

    def action(self, string):
        timestamp = time.strftime("[%H:%M] ", time.localtime(time.time()))
        str0 = escape_colorize(string[0])
        str1 = escape_colorize(string[1])
        tmpl = tempita.Template.from_filename(self.root+inspect.stack()[0][3], namespace=filters, encoding=None)
        return tmpl.substitute(locals()).replace("\n", "")

    def list(self, li):
        li = [escape_colorize(_) for _ in li]
        tmpl = tempita.Template.from_filename(self.root+inspect.stack()[0][3], namespace=filters, encoding=None)
        return tmpl.substitute(locals()).replace("\n", "")

    def topic(self, string):
        str0 = escape_colorize(string)
        tmpl = tempita.Template.from_filename(self.root+inspect.stack()[0][3], namespace=filters, encoding=None)
        return tmpl.substitute(locals()).replace("\n", "")

    def topic_changed(self,string):
        timestamp = time.strftime("[%H:%M] ", time.localtime(time.time()))
        str0 = escape_colorize(string)
        tmpl = tempita.Template.from_filename(self.root+inspect.stack()[0][3], namespace=filters, encoding=None)
        return tmpl.substitute(locals()).replace("\n", "")

    def tab_li(self, string):
        chan = escape_colorize(string[0])
        id = escape_colorize(string[1])
        tmpl = tempita.Template.from_filename(self.root+inspect.stack()[0][3], namespace=filters, encoding=None)
        return tmpl.substitute(locals()).replace("\n", "")

    def tab_div(self, string):
        id = escape_colorize(string)
        tmpl = tempita.Template.from_filename(self.root+inspect.stack()[0][3], namespace=filters, encoding=None)
        return tmpl.substitute(locals()).replace("\n", "")

class Irc_Client(irc.IRCClient):

    def __init__(self, *args, **kwargs):
        self._namescallback = {}

    def connectionMade(self):
        global STATUS
        self.versionName = "Magic Irc"
        self.versionNum = "0.2.8dev"
        self.nickname = self.factory.nickname
        self.nick_dict = {}
        self.topic_dict = {}
        self.html_wrapper = Html_Wrapper()
        irc.IRCClient.connectionMade(self)

    def connectionLost(self, reason):
        irc.IRCClient.connectionLost(self, reason)

    def signedOn(self):
        global id_chan_srv_dict
        ppc = dict([(v[0],k) for (k,v) in id_chan_srv_dict.iteritems()])
        for c in self.factory.chanlist:
            id_chan_srv_dict[ppc[c]] = (c, self)
            self.join(c)    
        del ppc

    def joined(self, channel):
        fill_recv( self.html_wrapper.join((self.nickname, channel)), get_channel_id(channel, self) )
        self.names(channel).addCallback(self.got_names, channel)

    def got_names(self, nicklist, channel):
        nicklist = filter(None, nicklist)
        self.nick_dict[channel] = nicklist
        fill_recv(  self.html_wrapper.list(nicklist), get_channel_id( channel, self ), msgtype = "userlist" )

    def userJoined(self, user, channel):
        fill_recv( self.html_wrapper.join((user, channel)), get_channel_id( channel, self )  )
        self.names(channel).addCallback(self.got_names, channel)

    def userLeft(self, user, channel):
        fill_recv( self.html_wrapper.part((user, channel)), get_channel_id( channel, self )  )
        self.names(channel).addCallback(self.got_names, channel)

    def userQuit(self, user, quitMessage):
        for key in self.nick_dict.keys():
            if user in self.nick_dict[key]:
                fill_recv( self.html_wrapper.quit((user, quitMessage)), get_channel_id( key, self ) )
                self.names(key).addCallback(self.got_names, key)

    def kickedFrom(self, channel, kicker, message):         
        fill_recv( self.html_wrapper.me_kicked((channel, kicker, message)), get_channel_id( channel, self )  )
        self.names(channel).addCallback(self.got_names, channel)

    def userKicked(self, kickee, channel, kicker, message):
        fill_recv( self.html_wrapper.kicked((kickee, channel, kicker, message)), get_channel_id( channel, self )  )
        self.names(channel).addCallback(self.got_names, channel)    

    def topicUpdated(self, user, channel, newTopic):
        fill_recv( self.html_wrapper.topic(newTopic), get_channel_id( channel, self ), msgtype = "topic" )
        fill_recv( self.html_wrapper.topic_changed(user), get_channel_id( channel, self ))
        self.topic_dict[channel] = (newTopic, user)

    def get_topic(self, channel):
        try:
            fill_recv( self.html_wrapper.topic(self.topic_dict[channel][0]), get_channel_id( channel, self ), msgtype = "topic" )
            if self.topic_dict[channel][0] != ' ': #magic space
                fill_recv( self.html_wrapper.topic_changed(self.topic_dict[channel][1]), get_channel_id( channel, self ))
        except:
            self.topic_dict[channel] = (' ', '') #magic space here
            fill_recv( self.html_wrapper.topic(self.topic_dict[channel][0]), get_channel_id( channel, self ), msgtype = "topic" )

    def exit_channel(self, channel):  #clean exit
            self.leave(channel)
            self.factory.chanlist.remove(channel)
            del self.nick_dict[channel]
            del self.topic_dict[channel]

    def privmsg(self, user, channel, msg):
        user = user.split('!', 1)[0]
        if msg:
            if channel.lower() == self.nickname.lower():
                if user in [ _[0] for _ in id_chan_srv_dict.values() ]:
                    fill_recv(  self.html_wrapper.privmsg((user, msg)), get_channel_id( user, self )  )
                else:
                    new_tab = max(id_chan_srv_dict) + 1
                    id_chan_srv_dict.update( { new_tab : (user, self) } )
                    fill_recv( user, new_tab, msgtype = 'addtab' )
                    fill_recv( self.html_wrapper.privmsg((user, msg)), get_channel_id( user, self )  )

            elif msg.startswith(self.nickname):
                fill_recv(  self.html_wrapper.privmsg_highlight((user, msg)), get_channel_id( channel, self )  )    
            else:
                fill_recv(  self.html_wrapper.privmsg((user, msg)), get_channel_id( channel, self )  )      

    def action(self, user, channel, msg):
        user = user.split('!', 1)[0]
        fill_recv( self.html_wrapper.action((user, msg)), get_channel_id( channel, self ) )

    def irc_NICK(self, prefix, params):
        old_nick = prefix.split('!')[0]
        new_nick = params[0]
        for key in self.nick_dict.keys():
            for n in self.nick_dict[key]:
                if ((n[0] == "@" or "+" or "~") and n[1:] == old_nick) or (n == old_nick) :
                        fill_recv( self.html_wrapper.nickchange((old_nick, new_nick)), get_channel_id( key, self ) )
                        self.names(key).addCallback(self.got_names, key)

    def send_stream(self, channel, msg):
        self.msg(channel, msg)
        fill_recv( self.html_wrapper.privmsg((self.nickname, msg)), get_channel_id( channel, self ) )

    def names(self, channel):
        channel = channel.lower()
        d = defer.Deferred()
        if channel not in self._namescallback:
            self._namescallback[channel] = ([], [])
        self._namescallback[channel][0].append(d)
        self.sendLine("NAMES %s" % channel)
        return d

    def irc_RPL_NAMREPLY(self, prefix, params):
        channel = params[2].lower()
        nicklist = params[3].split(' ')
        if channel not in self._namescallback:
            return
        n = self._namescallback[channel][1]
        n += nicklist

    def irc_RPL_ENDOFNAMES(self, prefix, params):
        channel = params[1].lower()
        if channel not in self._namescallback:
            return
        callbacks, namelist = self._namescallback[channel]  
        for cb in callbacks:
            cb.callback(namelist)
        del self._namescallback[channel]

    def pong(self, user, secs): 
        return

    def noticed(self, user, channel, message):
        global STATUS
        if len(STATUS) > 40: STATUS = STATUS[:20]
        fill_recv(  self.html_wrapper.privmsg(("NOTICE", ' '.join([user, channel, message]))), ROOTNAME  )
        STATUS += [("NOTICE", ' '.join([user, channel, message]))]


    def receivedMOTD(self, motd):
        global STATUS
        if len(STATUS) > 40: STATUS = STATUS[:20]
        for _ in motd:
            fill_recv(  self.html_wrapper.privmsg(("MOTD", _)), ROOTNAME  )
            STATUS += [("MOTD", _)]

    def irc_unknown(self, prefix, command, params):
        global STATUS
        if len(STATUS) > 40: STATUS = STATUS[:20]
        fill_recv(  self.html_wrapper.privmsg(("UNKNOWN", prefix+' '+command+' '+' '.join([_ for _ in params ]))), ROOTNAME  )
        STATUS += [("UNKNOWN", prefix+' '+command+' '+' '.join([_ for _ in params ]))]
        #fill_recv(  self.html_wrapper.privmsg((user, msg)), get_channel_id( channel, self )  )
        #print 'UNKNOWN:', prefix, command, params

    def irc_ERR_ERRONEUSNICKNAME(self, prefix, params):
        global STATUS
        if len(STATUS) > 40: STATUS = STATUS[:20]
        fill_recv(  self.html_wrapper.privmsg(("ERRONEUSNICKNAME", prefix+' '.join([_ for _ in params ]))), ROOTNAME  )
        STATUS += [("UNKNOWN", prefix+' '+' '.join([_ for _ in params ]))]
        #print prefix
        #print params

class Irc_Connect_Factory(protocol.ClientFactory):

    def __init__(self, profile, chanlist):
        self.nickname = profile["nickname"].encode("utf-8")
        self.chanlist = chanlist

    def buildProtocol(self, addr):
        self.p = Irc_Client()
        self.p.factory = self
        return self.p

    def clientConnectionLost(self, connector, reason):
        #connector.connect()
        reactor.stop()
    
    def clientConnectionFailed(self, connector, reason):
        print "IRC connection failed:", reason
        connector.connect()
        #reactor.stop()

class WsProtocol(protocol.Protocol):

    def connectionMade(self):
        global ws_instance
        ws_instance = self

    def dataReceived(self, data):
        tmp = json.loads(data)['data']
        if tmp["data"][0] == "/":
            command = tmp["data"].split(' ')[0]
            try:
                params = tmp["data"].split(' ')[1:]
            except:
                pass
            try:
                handle_commands(command, int(tmp['tab']), params)
            except Exception, e:
                print "FIXME", e    
        elif json.loads(data)["type"] == "control":
            controller(tmp["data"], tmp["tab"])
        else:
            try:
                fill_stream(tmp['data'], int(tmp['tab']))
            except Exception, e:
                print "FIXME", e    

    def send_to_browser(self, data, tab, msgtype):
        tmp = '{"type":"'+msgtype+'", "data":'+data+', "tab":"#tabs-'+str(tab)+'"}' 
        self.transport.write(tmp)

class WsFactory(protocol.Factory):
    def buildProtocol(self, addr):
        if addr.host == "127.0.0.1":
            return WsProtocol() 

class Web_Interface():
    def multiconnection(self):
        global id_chan_srv_dict
        global config
        id_chan_srv_dict = {}
        for server in config["servers"]:
            chanlist = server["chanlist"]
            profile = server["profile"]
            for ch in config["chanlists"]:
                if ch["chanlist"] == chanlist:
                    ok_chlist = [ ( _, '') for _ in ch["channels"] ]
                    a = len(ok_chlist)
                    tmp_len = len(id_chan_srv_dict)
                    id_chan_srv_dict.update(dict(zip( [ _ + tmp_len for _ in range(*[0,a])] , ok_chlist )))
            for pr in config["profiles"]:
                if pr["profile"] == profile:
                    ok_profile = pr
            factory = Irc_Connect_Factory(ok_profile, [ _[0] for _ in ok_chlist ])
            if config["use_http_proxy"]:
                proxy = HTTPProxyConnector(config["http_proxy_addr"], config["http_proxy_port"])
                if server["ssl"]:
                    proxy.connectSSL( server["address"], server["port"], factory, ssl.ClientContextFactory() )
                else:
                    proxy.connectTCP( server["address"], server["port"], factory )
            else:
                if server["ssl"]:
                    reactor.connectSSL( server["address"], server["port"], factory, ssl.ClientContextFactory() )
                else:
                    reactor.connectTCP( server["address"], server["port"], factory )

    def reactor_wsgi(self):
        resource = Tw_Server_Resource()
        site = Site(resource)
        ws_factory = WsFactory()
        self.multiconnection()
        reactor.listenTCP(8007, WebSocketFactory(ws_factory) )
        reactor.listenTCP(1753, site)
        reactor.run()

    def start(self):
        self.p = Process(target = self.reactor_wsgi)
        self.p.start()

    def stop(self):
        if self.p or self.p.is_alive():
            self.p.terminate()
        sys.exit(1)

def main(config_file = 'config.txt'):
    global config

    try:
        f = open(config_file, "r")
        config = eval(f.read())
        f.close()
    except:
        sys.exit("Config file error")

    iface = Web_Interface()
    iface.start()

    if config["gui"]:
        from gui import Gui_Creator
        GUI = Gui_Creator(sys.argv)
        iface.stop()

if __name__ == "__main__":
    main()
