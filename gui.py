#!/usr/bin/python
# -*- coding: utf-8 -*-

try:
	from PySide import QtGui, QtWebKit, QtNetwork
	from PySide.QtCore import QUrl, Qt
except ImportError:
	from PyQt4 import QtGui, QtWebKit, QtNetwork
	from PyQt4.QtCore import QUrl, Qt

class Gui_Creator():

		def __init__(self, args):
			self.args = args
			self.app = QtGui.QApplication(self.args)
			self.app.setStyle('gtk')
			if not QtGui.QSystemTrayIcon.isSystemTrayAvailable():
				QtGui.QMessageBox.critical(None, "Oersee", "I couldn't detect any system tray on this system.")
				sys.exit(1)
			QtGui.QApplication.setQuitOnLastWindowClosed(False)
			self.window = Window()
			self.window.show()
			self.app.exec_()

class Window(QtWebKit.QWebView):
	def __init__(self):
		super(Window, self).__init__()
		self.createActions()
		self.trayIconPixmap = QtGui.QIcon('chocat.png')
		self.createTrayIcon()
		self.trayIcon.setIcon( self.trayIconPixmap )
		self.trayIcon.show()
		self.setWindowTitle("Oersee")
		#self.setWindowFlags(Qt.FramelessWindowHint)
		self.resize(656, 355)
		self.load(QUrl("http://localhost:1753"))

	def setVisible(self, visible):
		self.minimizeAction.setEnabled(visible)
		self.maximizeAction.setEnabled(not self.isMaximized())
		self.restoreAction.setEnabled(self.isMaximized() or not visible)
		super(Window, self).setVisible(visible)

	def closeEvent(self, event):
		if self.trayIcon.isVisible():
			self.hide()
			event.ignore()

	def createActions(self):
		self.minimizeAction = QtGui.QAction("Mi&nimize", self, triggered=self.hide)
		self.maximizeAction = QtGui.QAction("Ma&ximize", self, triggered=self.showMaximized)
		self.restoreAction = QtGui.QAction("&Restore", self, triggered=self.showNormal)
		self.quitAction = QtGui.QAction("&Quit", self, triggered=QtGui.qApp.quit)

	def createTrayIcon(self):
		self.trayIconMenu = QtGui.QMenu(self)
		#self.trayIconMenu.addAction(self.minimizeAction)
		self.trayIconMenu.addAction(self.maximizeAction)
		self.trayIconMenu.addAction(self.restoreAction)
		self.trayIconMenu.addSeparator()
		self.trayIconMenu.addAction(self.quitAction)
		self.trayIcon = QtGui.QSystemTrayIcon(self)
		self.trayIcon.setContextMenu(self.trayIconMenu)