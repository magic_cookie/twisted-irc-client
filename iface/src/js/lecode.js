var ws = $.websocket("ws://127.0.0.1:8007/", {
        open: function() { },
        close: function() { alert("websocket closed"); },
        events: {
                message: function(e) { render(e.data, e.tab);},
                userlist: function(e) { render_userlist(e.data, e.tab); },
                topic: function(e) { topic(e.data, e.tab); },
                addtab: function(e) { addtab(e.data, e.tab); },
                removetab: function(e) { removetab(e.data, e.tab); }
        }
});

function scrolldown(tab){
$(tab+' .irc').scrollTop($(tab+' .irc')[0].scrollHeight);
//$('#irc').animate({scrollTop: $('#irc').get(0).scrollHeight}, 1000);
};

$(document).on('click', '.nickname', function(){
var nick = $(this).html()    
var tab = $(this).parents('div[id^=tabs-]').eq(0).attr('id')
$('#'+tab+' .send_input').val( nick+': '+$('#'+tab+' .send_input').val())
});

$(document).on('click', '.btn_send', function (e) {
    e.preventDefault();
    var div = $(this).parents('div[id^=tabs-]').eq(0).attr('id')
    var t = div.substring(5);
    var asdf = $('#'+div+' .send_input').val();
    $('#'+div+' .send_input').val('');
    if (asdf){ ws.send('message', {data:asdf, tab:t}); };
});

function render(data, tab){
$(tab+' .irc').html( $(tab+' .irc').html() + data ); scrolldown(tab);
};

function render_userlist(data, tab){
$(tab+' .ulist').html( data );
};

function topic(data, tab){
$(tab+' .topicbar').html( data );
};

function addtab(data, tab){
    tab = tab.substring(6);
    var tabs = $( "#tabs" ).tabs();
    tabs.find( ".ui-tabs-nav" ).append( data.new_tab );
    tabs.append( data.new_div );
    tabs.tabs( "refresh" );
    tabs.tabs("option", "active", -1);
};

function removetab(data, tab){
    $( "#tabs a[href='"+tab+"']").parent().remove();
    $( tab ).remove();
};

$( "#tabs" ).tabs({
    activate: function( e, ui ) {
        check_divs($('.ui-tabs-active > a').attr("href"));
    }
});

function check_divs(tab) {
if ( !isNaN(parseInt(tab.substring(6))) && $(tab+" .ulist").html()==''){ws.send('control', {data:'request_nicklist', tab:tab.substring(6)})}
if ( !isNaN(parseInt(tab.substring(6))) && $(tab+" .topicbar").html()==''){ws.send('control', {data:'request_topic', tab:tab.substring(6)})}
if ( tab.substring(6) =="-root" && $(tab+" .irc").html()==''){ws.send('control', {data:'request_status', tab:tab.substring(6)})}
scrolldown(tab);
};

$(function() {
        $(document).on("click", "#tabs span.ui-icon-close", function() {
            var panelId = $( this ).closest( "li" ).remove().attr( "aria-controls" );
            var t = panelId.substring(5)
            $( "#" + panelId ).remove();
            ws.send('control', {data:'exit_channel', tab:t});
            tabs.tabs( "refresh" );
        });
});

$(function() {
var tabs = $( "#tabs" ).tabs();
    tabs.find( ".ui-tabs-nav" ).sortable({
        axis: "x",
        stop: function() {
            tabs.tabs( "refresh" );
        }
    });
});

$(function() {
$( "button" ).button({
            icons: {
                primary: "ui-icon-gear",
                secondary: "ui-icon-triangle-1-s"
            },
            text: false
        })
});